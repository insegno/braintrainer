package com.matteobad.braintrainer;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // option buttons
    ArrayList<Button> options;

    // header textViews
    TextView timerTextView;
    TextView questionTextView;
    TextView scoreTextView;
    // message on the bottom
    TextView messageTextView;
    // play again button
    Button playAgainButton;

    private int correctAnswer;
    private int correctPos;
    private int correctAnswers;
    private int totalQuestions;

    public void play(View view) {
        Button playButton = (Button) findViewById(R.id.playButton);
        LinearLayout game = (LinearLayout) findViewById(R.id.brainerLinearLayout);
        playButton.setVisibility(View.GONE);
        game.setVisibility(View.VISIBLE);

        this.resetGame();
        this.startGame();

    }

    public void playAgain(View view) {
        this.resetGame();
        this.startGame();
    }

    public void choose(View view) {
        Button button = (Button) view;
        this.totalQuestions++;
        if (Integer.valueOf(button.getText().toString()) == this.correctAnswer) {
            messageTextView.setText("Correct Answer!");
            this.correctAnswers++;
        } else {
            messageTextView.setText("Wrong Answer!");
        }

        scoreTextView.setText(String.valueOf(correctAnswers + "/" + totalQuestions));
        this.generateQuestion();
    }

    private void startGame() {
        this.generateQuestion();

        new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long secondsUltimFinished = millisUntilFinished / 1000;
                timerTextView.setText(String.valueOf(secondsUltimFinished));
            }

            @Override
            public void onFinish() {
                timerTextView.setText("0");
                playAgainButton.setVisibility(View.VISIBLE);
                messageTextView.setText(String.valueOf("Score: " + correctAnswers + "/" + totalQuestions));
                for (Button button: options) button.setEnabled(false);
            }
        }.start();
    }

    private void generateQuestion() {
        int op1 = (int) (Math.random() * 50);
        int op2 = (int) (Math.random() * 50);
        this.correctAnswer = op1 + op2;
        questionTextView.setText(String.valueOf(op1 + " + " + op2));

        for (int i = 0; i < 4; i++) {
            int option = (int) (Math.random() * 100);
            options.get(i).setText(String.valueOf(option));
        }

        this.correctPos = (int) (Math.random() * 4);
        options.get(this.correctPos).setText(String.valueOf(this.correctAnswer));
    }

    private void resetGame() {
        this.correctAnswers = 0;
        this.totalQuestions = 0;
        playAgainButton.setVisibility(View.GONE);
        timerTextView.setText("30");
        scoreTextView.setText("0/0");
        messageTextView.setText("");
        for (Button button: options) button.setEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timerTextView = (TextView) findViewById(R.id.timerTextView);
        questionTextView = (TextView) findViewById(R.id.questionTextView);
        scoreTextView = (TextView) findViewById(R.id.scoreTextView);

        options = new ArrayList<>();
        options.add((Button) findViewById(R.id.option1Button));
        options.add((Button) findViewById(R.id.option2Button));
        options.add((Button) findViewById(R.id.option3Button));
        options.add((Button) findViewById(R.id.option4Button));

        messageTextView = (TextView) findViewById(R.id.messageTextView);

        playAgainButton = (Button) findViewById(R.id.playAgainButton);
    }
}
